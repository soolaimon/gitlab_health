require "gitlab_health/version"
require "gitlab_health/request_log"

module GitlabHealth
 def self.log_requests(args)
    args = defaults.merge(args)
    
    every = args[:every].to_i
    count = (args[:duration].to_i / every) + 1

    logs = count.times.map { GitlabHealth::RequestLog.new(uri: args[:uri]) }
    
    puts "Pinging #{args[:uri]} every #{args[:every]} seconds for #{args[:duration]} seconds."

    logs.each do |l|
      l.time_request
      puts "#{l.time_sent} | #{l.response_time} seconds"
      sleep(every) unless l == logs.last
    end
    
    puts "Average: #{GitlabHealth::RequestLog.average_response_time(logs)}"
 end

 private
 def self.defaults
  { uri: 'https://gitlab.com', duration: 60, every: 10 }
 end
end
