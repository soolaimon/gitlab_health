require 'net/http'
require 'benchmark'

class GitlabHealth::RequestLog
  attr_reader :uri, :time_sent, :response_time
  
  def self.average_response_time(logs)
    logs.map(&:response_time).reduce { |time, sum| sum += time} / logs.length    
  end
  
  def initialize(args = {})
    @uri = URI(args[:uri])
  end

  def time_request
    @time_sent = Time.now
    @response_time = Benchmark.realtime do
      Net::HTTP.get(uri)
    end
  end
end
