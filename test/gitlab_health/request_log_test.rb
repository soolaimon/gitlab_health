require "test_helper"
require 'net/http'

class GitlabHealth::RequestLogTest < Minitest::Test
  def setup
    @log = GitlabHealth::RequestLog.new(uri: 'https://gitlab.com')
  end

  def test_time_request_sends_get_request_to_uri
    Net::HTTP.expects(:get).with(URI('https://gitlab.com'))
    
    @log.time_request
  end

  def test_time_request_sets_a_response_time_and_time_sent
    @log.time_request
    assert_kind_of(Float, @log.response_time)
    assert_kind_of(Time, @log.time_sent)
  end

  def test_self_average_response_time_returns_average_of_log_response_times
    logs =  [
      mock('RequestLog', response_time: 1.0),
      mock('RequestLog', response_time: 0.0),
      mock('RequestLog', response_time: 0.5),
    ]
    assert_equal(GitlabHealth::RequestLog.average_response_time(logs), 0.5)
  end
end
