require "test_helper"

class GitlabHealthTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::GitlabHealth::VERSION
  end
  
  def test_log_response_logs_each_response_plus_average
    GitlabHealth.stubs(:sleep)
    assert_output(/Pinging https:\/\/gitlab.com every 1 seconds for 1 seconds.*/) do
      GitlabHealth.log_requests({every: 1, duration: 1})
    end
  end

  def test_passing_uri_pings_that_uri
    uri = 'https://about.gitlab.com'
    Net::HTTP.expects(:get).with(URI(uri)).at_least_once
    GitlabHealth.stubs(:sleep)
    assert_output(/Pinging #{uri} every 10 seconds for 60 seconds.*/) do
      GitlabHealth.log_requests(uri: uri)
    end
  end
end
