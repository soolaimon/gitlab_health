$LOAD_PATH.unshift File.expand_path("../../lib", __FILE__)
require "gitlab_health"
require "gitlab_health/request_log"

require "minitest/autorun"
require "mocha/minitest"
require "pry"


def setup
  Net::HTTP.stubs(:get).returns('blah response')
end
